import 'package:flutter/material.dart';
import '../scene/MainScene.dart';

void main() {
  runApp(new MyApp());
}

/*
 * 启动入口 <br/>
 * @link https://gitee.com/qlslylq/UnityDesign
 * @link https://gitee.com/qlslylq/FlutterDesign
 * @link https://gitee.com/qlslylq/AndroidDesign
 * @link https://gitee.com/qlslylq/JavaDesign
 */
class MyApp extends StatelessWidget {
  StatelessElement createElement() {
    asyncInit();
    return super.createElement();
  }

  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MainScene(),
    );
  }

  Future<void> asyncInit() async {
    try {} on Exception catch (e) {}
  }
}
