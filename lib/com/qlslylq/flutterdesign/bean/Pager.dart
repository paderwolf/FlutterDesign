import 'package:json_annotation/json_annotation.dart';

/*
 * 分页<br/>
 */
part 'Pager.g.dart';
@JsonSerializable()
class Pager {

  int totalCount; // 总数量

  int pageSize; // 每一页数量

  int pageNo; // 第几页

  int totalPage; // 总页数

  int currentPage; //pageNo

  int perPageRows; //pageSize

  Pager() {

  }

  factory Pager.fromJson(Map<String, dynamic> json) => _$PagerFromJson(json);

  Map<String, dynamic> toJson() => _$PagerToJson(this);
}
