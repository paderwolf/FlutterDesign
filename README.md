　　﻿作者：qlslylq (浅蓝深蓝叶落秋，https://www.bmob.cn/shop/detail/396)

　　联系：13297089301 QQ：2939143482

　　QQ讨论群：326550832(AndroidDesign)

　　名下开源框架：

　　1.AndroidDesign(eclipse，in 2014)

　　2.AndroidDesign(AndroidStudio，in 2016)

　　3.AdmxSDK及AdmxSDK_v2(AndroidStudio，in 2017)

　　4.UnityDesign(VisualStudio,C#，in 2017)

　　5.FlutterDesign(AndroidStudio，in 2019)

　　6.JavaDesign(Intellij IDEA，in 2020)：[查看项目](https://gitee.com/qlslylq/JavaDesign)

　　名下开源项目：

　　1.AndroidDesignQtfy (https://www.bmob.cn/shop/detail/396)

　　2.UnityDesignEasyAR (AR纹理识别demo)

　　3.UnityDesignThreeInOne (三合一游戏demo)

　　友情推广：

　　华为云：[华为云](https://account.huaweicloud.com/obmgr/invitation/invitation.html?bpName=0000000100000002ABF750871AEB87720ACC40C42A118E856B76DBBE60DBB4ED2ECFD88225E0C039F3BBD11759B3040456E0549FE8AE1850DE1FFF37FEB043717008C92DE61831DD&inviteCode=00000001000000021FF7919C82835DD65B03AF77AA85B03EC3EC9E3489913E124D96066CF783EC39&bindType=1&isDefault=1)

![华为云](https://images.gitee.com/uploads/images/2020/0928/130311_9b131343_452019.gif "huaweicloud.gif")

# FlutterDesign

#### 介绍
Flutter跨平台综合快速开发框架

#### 软件架构
使用传统的MV架构搭建窗口层及网络层。简单而易用，直接而快速。


#### 安装教程

1. using plugins with Dart sdk,Flutter sdk
2. using development tools with AndroidStudio or VisualStudio
3. using network library with dio

#### 使用说明

0. 运行入口文件：FlutterDesign\lib\com\qlslylq\flutterdesign\application\MyApp.dart
1. 窗口层，网络层的使用风格沿用名下AndroidDesign(AndroidStudio)框架风格
2. 由于所使用的应用窝后端云服务器关闭，影响了开发计划，诸多架构细节及组件库等后期逐渐调整
3. 由于计划被打乱及时间紧迫，不足之处及符合Flutter的生态各模块请各位自行增加，调整及优化！

#### 未来可更新的功能

　　1.窗口层网络进度栏的显示，隐藏，网络加载文本设置，关闭软键盘，访问控制优化

　　2.网络层Json解析的最佳方案修改，网络层及引擎优化，访问控制优化

　　3.常用组件库中常用组件的架构搭建，如原AdapterView架构搭建等，如ListView上层封装等

　　4.调试模块的建立，如AndroidDesign框架中的日志，内存，进程及控件调试模块；手机友好化网络日志的实时显示；隐式错误邮件发送；

　　5.与android,ios的native通信模块架构搭建

　　6.Socket网络层的架构及细节实现，封包拆包，半包沾包等细节处理，极量并发请求细节处理，可参考UnityDesign中的Socket网络层

　　7.非核心层(窗口层，网络层)的插件化分离，可自由选择是否使用，如常用组件库，调试模块插件

　　8.窗口层，网络层的接口化分离，将需求接口与定制化实现分离，使可以定制窗口层与网络层，如不使用dio等；需求接口与架构模式绑定，如编写MV架构接口，编写MVP架构接口，编写MVVM架构的接口等

　　9.xml/xaml/json/prefab或更高效率的结构化文件转widget组件树的插件实现

　　10.框架配置模块实现，使能够配置常用的主题布局，窗口样式，如是否显示左右侧滑，是否显示上下TabBar等，网络配置或其它等等

　　11.资源管理模块架构搭建，如文本，图片，颜色，尺寸，样式，属性，二进制资源，国际化资源等

　　12.兼容多端的本地轻量级存储，本地数据库持久化存储模块的建立；文件管理模块的建立

　　13.常用功能的添加，如下拉刷新及分页，左滑删除，时间友好化工具，数据缓存等

　　14.单例application的建立

　　15.路由模块的分离，跨级返回及传值的实现(参照AndroidDesign[AS版]中的ActivityManager)，路由管理及增删改的同步

　　16.跨平台到Web端的生态化重构

　　17.深入了解Dart及Flutter语言特性，如widget树随时重建，控件内存改变所带来的一系列架构变化，该以怎样的合适的重构架构来匹配这些大变化，整个架构的重新布局应该围绕哪些特点来展开

　　18.深入了解Dart及Flutter语言未来趋势，紧紧跟随及估测官方的发展路线，该抛弃的与可沿用的随时做好抉择与布局

　　19.各自为营与闭门造车只会加速行业乱象，行业标准化规范的制定与其统一生态体系的建立能够快速打通各不相干的软件体系，推动整个行业由线到面的过渡，由面到体的过渡；大量的技术点正在日以继夜地逐一突破，容纳，包含，以不变应万变能够带来生产效率的提高；其它行业如银行，金融体系都已建立了自己的行业标准，在连通各家体系的路上前进了一个维度。我的初衷便是如此，但一人之力何其渺小，技术不力复认知深锁，而无以祭献。但初心不改，希望在开源移动端综合快速开发框架的路上献出自己的一份力。

　　20.吾技术有限复时间微薄，还是希望大家踊跃提交自己的代码，共同建立FlutterDesign的各个生态体系吧！咱们一起成长~大争之世，吾自奋力！

#### 运行入口（截图为旧版，入口文件已调整到了application\MyApp.dart中，注意调整）

![输入图片说明](https://images.gitee.com/uploads/images/2019/0416/202412_b892c02c_452019.png "run.png")

#### 框架截图

![窗口层使用](https://gitee.com/uploads/images/2019/0406/210233_46ec8773_452019.png "Screenshot_2019-04-06-20-40-11-370_com.qlslylq.fl.png")

![窗口层使用](https://images.gitee.com/uploads/images/2019/0416/202848_9708529c_452019.jpeg "2.jpg")

![网络层使用](https://gitee.com/uploads/images/2019/0406/224852_a621f8fc_452019.png "Screenshot_2019-04-06-22-47-21-255_com.qlslylq.fl.png")